$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      intervals:100
    });
    $('#contacto').on('show.bs.modal',function(e){
      console.log('el modal se esta mostrando');
      $("[id='contactoBtn']").removeClass('btn-outline-primary')
      $("[id='contactoBtn']").addClass('btn-warning')

      $("[id='contactoBtn']").prop('disabled',true)

    });
    $('#contacto').on('shown.bs.modal',function(e){
      console.log('el modal se mostro');

    });
    $('#contacto').on('hide.bs.modal',function(e){
      console.log('el modal se oculta');

    });
    $('#contacto').on('hidden.bs.modal',function(e){
      console.log('el modal se oculto');
      $("[id='contactoBtn']").removeClass('btn-warning')
      $("[id='contactoBtn']").addClass('btn-outline-primary')
      $("[id='contactoBtn']").prop('disabled',false)

    });
}); 
