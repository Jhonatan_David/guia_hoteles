module.exports=function(grunt){
    grunt.initConfig({
        sass:{
           dist: {
               files: [{
                   expand: true,
                   cwd: 'css',
                   src: ['*.css'],
                   dest: 'css',
                   ext: '.css'
               }]
           } 
        },
        watch: {
            files:['css/*.css'],
            tasks:['css']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['sass']);
    

};